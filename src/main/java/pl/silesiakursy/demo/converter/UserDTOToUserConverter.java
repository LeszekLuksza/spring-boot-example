package pl.silesiakursy.demo.converter;

import pl.silesiakursy.demo.dto.UserDTO;
import pl.silesiakursy.demo.entity.User;

public class UserDTOToUserConverter {

    public static User convert(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        return user;
    }
}
